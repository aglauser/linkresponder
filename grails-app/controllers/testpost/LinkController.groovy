package testpost

class LinkController {

    def index() { }
    def post() {
    	def json = request.JSON
	def message = json.text
	def linkPattern = /\b(VSM)-(\d+)\b|\b(ICTAPPS)-(\d+)\b/ ///\bVSM-(\d+)\b|\bICTAPPS-\d+\b/
	def matches = message =~ linkPattern
	def idList = matches.collect { it[1] ? it[2] : it[3] + "-" + it[4] }
        def outMessage = "message is: "  + message + "\n it contains " + matches.count + " ticket numbers:"
	idList.each{ outMessage += " " + it }
	def response = [type: "message", text: outMessage]
	respond response
    }
}
